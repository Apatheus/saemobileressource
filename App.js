import React, { useEffect, useState } from 'react';
import { StyleSheet, StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './src/components/HomeScreen';
import IutBarcodeDemo from './src/components/IutBarcodeDemo';
import CircleTap from './src/components/CircleTap';
import Paramètres from './src/components/Paramètres';
import io from 'socket.io-client';
import { ThemeProvider, useThemeContext } from './src/components/ThemeContext';

const Stack = createNativeStackNavigator();

const App = () => {
  const [socket, setSocket] = useState(null);

  useEffect(() => {
    const Socket = io("localhost:9000", { transports: ['websocket'] });

    Socket.on('connect', () => {
      console.log('Connecté au serveur.');
    });

    setSocket(Socket);

    return () => {
      Socket.off('connect');
      Socket.close();
    };
  }, []);

  return (
    <ThemeProvider>
      <StatusBar barStyle="light-content" backgroundColor="#000" />
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="Accueil"
          screenOptions={screenOptions}
        >
          <Stack.Screen name="Accueil" component={HomeScreen} />
          <Stack.Screen name="QR Code" component={IutBarcodeDemo} />
          <Stack.Screen name="Circle Tap" component={CircleTap} />
          <Stack.Screen name="Paramètres" component={Paramètres} />
        </Stack.Navigator>
      </NavigationContainer>
    </ThemeProvider>
  );
};

const screenOptions = () => {
  const { navBarColor } = useThemeContext();

  return {
    headerStyle: {
      backgroundColor: navBarColor,
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 18,
    },
  };
};

export default App;
