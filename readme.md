# Bienvenue sur mon Readme

## Explications

**Cette application a été développée pour mobile**. En effet, le jeu que j'ai codé utilise l'accéléromètre de l'appareil pour déplacer l'entité contrôlée par le joueur. 

Le but du jeu est d'éviter les obstacles qui apparaissent depuis les bords de l'écran et de survivre le plus longtemps possible.
