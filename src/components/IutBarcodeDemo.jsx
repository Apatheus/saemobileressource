import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import Footer from './footer';
import Header from './header';

export default function IutBarcodeDemo() {
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [scannedType, setScannedType] = useState('');
    const [scannedData, setScannedData] = useState('');

    useEffect(() => {
        const getBarCodeScannerPermissions = async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        };

        getBarCodeScannerPermissions();
    }, []);

    const handleBarCodeScanned = ({ type, data }) => {
        setScanned(true);
        setScannedType(type);
        setScannedData(data);
    };

    if (hasPermission === null) {
        return <Text>Demande d'accès à la caméra.</Text>;
    }
    if (hasPermission === false) {
        return <Text>Pas d'accès à la caméra.</Text>;
    }

    return (
        <View style={styles.container}>
            <Header type={scannedType} data={scannedData} />
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
            />
            <View style={styles.scanner} />
            {scanned && <Button title={'Scanner un nouveau code ?'} onPress={() => setScanned(false)} />}
            <Footer />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    scanner: {
        zIndex: 1,
        borderWidth: 2,
        borderColor: 'white',
        borderStyle: 'dashed',
        borderRadius: 10,
        width: 250,
        height: 250,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -125,
        marginLeft: -125,
    },
});
