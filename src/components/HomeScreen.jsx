import React from 'react';
import { Image, View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import BoutonCustom from './BoutonCustom';

export default function HomeScreen({ navigation }) {

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Bienvenue sur Circle Dodge !</Text>
            <Image
                source={require('.././images/logo.png')}
                style={styles.logo}
                resizeMode="contain"
            />
            <BoutonCustom
                title="Lancer le jeu"
                onPress={() => navigation.navigate('Circle Tap')}
                style={styles.BoutonCustom}
            />
            <BoutonCustom
                title="Paramètres"
                onPress={() => navigation.navigate('Paramètres')}
                style={styles.BoutonCustom}
            />
            <TouchableOpacity onPress={() => navigation.navigate('QR Code')} style={styles.lienQRcode}>
                <Text style={styles.qrCodeText}>Scanner un QR Code</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        paddingBottom: '15%',
    },
    logo: {
        width: 200,
        height: 200,
        marginBottom: 20,
    },
    title: {
        fontSize: 28,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    lienQRcode: {
        position: 'absolute',
        bottom: 35,
    },
    qrCodeText: {
        color: 'black',
        fontSize: 14,
    },
});
