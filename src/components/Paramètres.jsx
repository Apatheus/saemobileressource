import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { useThemeContext } from './ThemeContext';

const ColorOption = ({ color, onChangeColor }) => {
    return (
        <TouchableOpacity
            onPress={() => onChangeColor(color)}
            style={[styles.colorOption, { backgroundColor: color }]}
        />
    );
};

const Paramètres = () => {
    const { setNavBarColor } = useThemeContext();

    // liste des couleurs que je veux utiliser
    const colors = ['#F4D03F', '#F5B041', '#F5B7B1', '#5499C7', '#27AE60', '#E74C3C', '#7D3C98', '#A04000', 'black'];

    const changeTheme = (color) => {
        setNavBarColor(color);
    };

    return (
        <View style={styles.container}>
            <View style={styles.title}>
                <Text style={styles.titleText}>Choisissez un thème pour l'application :</Text>
            </View>
            <View style={styles.colorGrid}>
                {colors.map((color, index) => (
                    <ColorOption key={index} color={color} onChangeColor={changeTheme} />
                ))}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        marginBottom: 20,
    },
    titleText: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    colorGrid: { // Pour former un carré
        width: 180,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    colorOption: {
        width: 50,
        height: 50,
        borderRadius: 25,
        margin: 5,
    },
});

export default Paramètres;
