import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useThemeContext } from './ThemeContext';

export default function BoutonCustom({ onPress, title }) {
    const { navBarColor } = useThemeContext();

    return (
        <TouchableOpacity onPress={onPress} style={[styles.button, { backgroundColor: navBarColor }]} >
            <Text style={styles.text}>{title}</Text>
        </TouchableOpacity >
    );
}

const styles = StyleSheet.create({
    button: {
        elevation: 8,
        borderWidth: 3,
        borderColor: "white",
        paddingVertical: 10,
        paddingHorizontal: 12,
        margin: 5
    },
    text: {
        fontSize: 18,
        color: "#fff",
        fontWeight: "bold",
        alignSelf: "center",
    }
});
