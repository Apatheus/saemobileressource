import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Footer = () => {
    return (
        <View style={styles.footer}>
            <Text style={styles.footerText}>Damien Bachelier</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    footer: {
        height: 65,
        width: '100%',
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
    },
    footerText: {
        color: 'white',
    },
});

export default Footer;
