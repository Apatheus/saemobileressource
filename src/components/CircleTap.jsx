import React, { useState, useEffect } from 'react';
import { SafeAreaView, View, TouchableOpacity, Text, StyleSheet, Dimensions } from 'react-native';
import { Accelerometer } from 'expo-sensors';

const circleSize = 30;
const gameAreaMargin = 40;
const gameAreaWidth = Dimensions.get('window').width - gameAreaMargin * 2;
const gameAreaHeight = 300;
const obstacleSize = 20;

const GameCircle = ({ position }) => {
    return <View style={[styles.circle, position]} />;
};

const createObstacle = () => {
    const side = Math.floor(Math.random() * 2); // 0: gauche, 1: droite
    let position, direction;

    if (side === 0) { // Gauche
        position = { top: Math.random() * (gameAreaHeight - obstacleSize), left: -obstacleSize };
        direction = { x: 1, y: 0 };
    } else { // Droite
        position = { top: Math.random() * (gameAreaHeight - obstacleSize), left: gameAreaWidth };
        direction = { x: -1, y: 0 };
    }

    return { position, direction };
};

const CircleTap = () => {
    const [circlePosition, setCirclePosition] = useState({ top: gameAreaHeight / 2, left: gameAreaWidth / 2 });
    const [isJumping, setIsJumping] = useState(false);
    const [obstacles, setObstacles] = useState([]);
    const [gameOver, setGameOver] = useState(false);

    useEffect(() => {
        const obstacleInterval = setInterval(() => {
            if (!gameOver) {
                setObstacles(currentObstacles => [...currentObstacles, createObstacle()]);
            }
        }, 2000);

        return () => clearInterval(obstacleInterval);
    }, [gameOver]);

    useEffect(() => {
        const obstacleMoveInterval = setInterval(() => {
            setObstacles(currentObstacles => {
                return currentObstacles.map(obstacle => {
                    const newTop = obstacle.position.top + obstacle.direction.y * 5;
                    const newLeft = obstacle.position.left + obstacle.direction.x * 5;
                    return { ...obstacle, position: { top: newTop, left: newLeft } };
                }).filter(obstacle => obstacle.position.left > -obstacleSize && obstacle.position.left < gameAreaWidth + obstacleSize);
            });
        }, 50);

        return () => clearInterval(obstacleMoveInterval);
    }, []);

    useEffect(() => {
        Accelerometer.setUpdateInterval(400);

        const subscription = Accelerometer.addListener(({ x }) => {
            requestAnimationFrame(() => {
                const deltaX = x * 20;
                let newLeft = Math.max(0, Math.min(circlePosition.left + deltaX, gameAreaWidth - circleSize));
                let newTop = isJumping ? Math.max(0, circlePosition.top - 10) : Math.min(gameAreaHeight - circleSize, circlePosition.top + 5);

                obstacles.forEach(obstacle => {
                    if (newLeft < obstacle.position.left + obstacleSize &&
                        newLeft + circleSize > obstacle.position.left &&
                        newTop < obstacle.position.top + obstacleSize &&
                        newTop + circleSize > obstacle.position.top) {
                        setGameOver(true);
                    }
                });

                setCirclePosition({ top: newTop, left: newLeft });
            });
        });

        return () => subscription.remove();
    }, [circlePosition, isJumping, obstacles]);

    const handleJumpPress = () => setIsJumping(true);
    const handleJumpRelease = () => setIsJumping(false);

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.gameArea}>
                <GameCircle position={circlePosition} />
                {obstacles.map((obstacle, index) => (
                    <View key={index} style={[styles.obstacle, obstacle.position]} />
                ))}
            </View>
            {!gameOver && (
                <TouchableOpacity
                    onPressIn={handleJumpPress}
                    onPressOut={handleJumpRelease}
                    style={styles.jumpButton}
                >
                    <Text style={styles.jumpButtonText}>Jump</Text>
                </TouchableOpacity>
            )}
            {gameOver && (
                <View style={styles.gameOverContainer}>
                    <Text style={styles.gameOverText}>Game Over</Text>
                </View>
            )}
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    gameArea: {
        width: gameAreaWidth,
        height: gameAreaHeight,
        borderWidth: 5,
        borderColor: 'white',
        position: 'relative',
        backgroundColor: 'black',
    },
    circle: {
        position: 'absolute',
        width: circleSize,
        height: circleSize,
        backgroundColor: '#FF0000',
        borderRadius: circleSize / 2,
    },
    obstacle: {
        position: 'absolute',
        width: obstacleSize,
        height: obstacleSize,
        backgroundColor: 'white',
    },
    jumpButton: {
        position: 'absolute',
        bottom: gameAreaHeight - 180,
        alignSelf: 'center',
        backgroundColor: '#FF0000',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 5,
    },
    jumpButtonText: {
        color: 'white',
        fontSize: 20,
    },
    gameOverContainer: {
        position: 'absolute',
        alignSelf: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        padding: 10,
        borderRadius: 10,
    },
    gameOverText: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
    },
});

export default CircleTap;
