import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = ({ type, data }) => {
    console.log('Header rendering', { type, data });
    return (
        <View style={styles.header}>
            <Text style={styles.headerText}>Informations du QR Code</Text>
            {type && <Text style={styles.headerText}>Type : {type}</Text>}
            {data && <Text style={styles.headerText}>Données : {data}</Text>}
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        zIndex: 1,
        height: 120,
        width: '100%',
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'center',
    },

    headerText: {
        color: 'white',
        fontSize: 16,
    },
});

export default Header;
