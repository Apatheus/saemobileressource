import React, { createContext, useState, useContext } from 'react';

const ThemeContext = createContext();

export const useThemeContext = () => useContext(ThemeContext);

export const ThemeProvider = ({ children }) => {
    const [navBarColor, setNavBarColor] = useState('black'); // le thème par défaut
    const [appTheme, setAppTheme] = useState();

    return (
        <ThemeContext.Provider value={{ navBarColor, setNavBarColor, appTheme, setAppTheme }}>
            {children}
        </ThemeContext.Provider>
    );
};
